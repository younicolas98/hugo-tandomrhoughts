---
title: "Comments!"
subtitle: "Comment section works frfr"
date: 2023-08-22T22:40:02-03:00
draft: false
tags: ["Tech", "Blog"]
---

Hey guys, quick update. 

Now you can comment on my posts (frfr). They can be anonymous. You have to wait for the admin (me) to approve them.

![comments_screenshot](/post/2023-08-22_comments.png)

FYI, I'm using [Cusdis](https://cusdis.com/). It has a free subscription (yay!), 100 comments per month limited to 1 site. I replaced the previous commenting system, Disqus. It was shitty, as it needed readers to login. 

That's it for now. 

I'm still thinking about my next post. It might be about kiosa or why I like card tricks, idk tho 🤷.