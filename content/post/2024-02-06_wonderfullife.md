---
title: "Tandom Rhoughts, Movie Edition"
subtitle: "Some thoughts on Kore-eda's After Life (1998)"
date: 2024-06-02T01:31:01-03:00
tags: ["Movies"]
draft: false
---

# What makes a movie good? 

A highly subjetive matter that can be answered in many ways. Some might say it should be focused on pure entertainment. I'd say a good movie that lingers on your head asks questions, rather than giving answers.

Today we're going to talk about "After Life" (1998) also known as _Wonderful Life_ directed by Kore-eda Hirokazu.  

![poster](/post/2024-06-02/poster.png)

## The Story

After a brief prologue, as soft bells resonate, a person walks in through the open door. It is someone who has just died, entering this world between life and death to stay for a while. "After Life" is a story that unfolds over a week in that world.
{{< youtube _n8agqeI1qY >}}

Kore-eda sets up the first half with interviews of the deceased. They are asked the question following question: _if you had to choose the single best memory of your life, what would it be?_ Is it a happy one? A sad one? A childhood memory? 

![protagonists](/post/2024-06-02/shioriandmochizuki.png "Our main characters: Shiori (left) & Mochizuki (right)")


## Style

Director Kore-eda started his career with documentaries. So it's natural that he chose to tell us a fictional story with a documentary-like approach. This is done by following one rule: *the camera moves if the character moves*.

During the interviews, he uses a static shot.

![stationary](/post/2024-06-02/stationary.png)

If there's movement, we change to a handheld shot. As we can see during the opening prologue.

Like the story, the cinematic choices (such as art direction and design) are also minimalistic while adding up to the warm tone of the movie.

![phone](/post/2024-06-02/phone.png)

They even shot on location (an abandoned school building). Had they constructed a set, it wouldn't have the same feel.

![school](/post/2024-06-02/school.png)

## Show, don't tell

There's the primordial rule of good storytelling. _"Show, don't tell"_

I found it funny that for the first half of the movie, it doesn't seem to follow it. The interviews are dialogue heavy. There's no flashback scene of the memories. 

It's from the halfway point that we change perspective. Focus goes from the documentary style interviews to our main characters Mochizuki & Shiori. This happens in a specific point around the 45 minute mark. In this shot, perspective flips 180 and we start to follow the storyline of the crew.

![45min](/post/2024-06-02/45min.png)

## Second Half

From this point onward, we get closer to our characters. With handheld shots which feel more personal. In contrast of the colder static shots of the interviews.

{{< youtube AaTyTufmq8A >}}

The story then follows Mochizuki and Shiori and the crew working on the films. 

![set](/post/2024-06-02/set.png)

![band](/post/2024-06-02/band.png)


The movie also has two recurring themes: the bench and looking up to the Moon.

![bench](/post/2024-06-02/bench.png)

![bench2](/post/2024-06-02/bench2.png)

![moon1](/post/2024-06-02/moon1.png)

![moon](/post/2024-06-02/moon.png)


We don't get to see the finished films of the memories people chose. But that's not the point. As the real story is about our main characters. Mochizuki choosing his best memory of his life.

{{< youtube qSQTVDEFPVI >}}

And Shiori following his steps as he leaves this world.

## Closing thoughts

Kore-eda's _After Life_ is much more interesting than entertaining. But that doesn't mean it isn't enjoyable. There's the idea that a really good movie starts after the credit rolls. 

It's quite difficult to choose just one memory to keep forever. I wouldn't even know where to begin. Should it be about my family? Friends? Someone I loved? Or the small things in life? Like the 60 seconds I get to enjoy during my commute to my boring ass job. Where I get to drive for 70 km/h clear of traffic, trees by the side of the slightly curved road, windows down and the feel the wind flowing.

So, if _you_ had to choose the single best memory of your life, what would it be?

Thanks for reading :)

Kore-eda's other films I've watched and recommend:

- _Like Father, Like Son_ (2013). A man realizes what it means to be a father.
- _Still Walking_ (2008). What's family for you?

Other movies with similar themes:

- _Soul_ (2020). A Pixar movie with deep questions about the meaning of life.
- _Be Kind, Rewind_ (2008). Jack Black and Mos Def recreate bootleg versions of famous movies.
