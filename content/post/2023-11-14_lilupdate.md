---
title: "lil Update 🎶 "
date: 2023-11-14T23:43:28-03:00
draft: false
tags: ["Random Thoughts"]
---
![Club penguin](/post/2023-11-15_clubpenguin.jpg)
Hey guys, surprise surprise tandom-rhoughts is still alive (dunno why I'm writing in English, but fuck it 🤷). Just wanted to share what I've been up to. 

## lil Update deserves a lil song 🎶

{{< spotify type="track" id="12CNybio0Bxean3F3uYugU" width="100%" height="152" >}}

Contrary to Smino's track, today I took a [Major L](https://www.urbandictionary.com/define.php?term=Major%20L). Tomorrow, my 6 month long trainee program ends and I failed to get a full time job offer. 

Bad news always suck. The thing that hurt me most is that I was kept in the dark up until the (almost) very last moment. The feedback I got was "you did great but current situation with the merge new hires are not a priority". I guess that counts as a half-win. Boss (Mr. C) told me that he'll open a position that fits my profile and was held back by the merge as soon as it finishes next year. Hope that's true and not only sweet talk. 

{{< youtube 49ZTp_vgI8o >}}

It's funny that a couple days ago this video popped in my youtube subscriptions feed. I guess I can relate to it now. haha :(

I'll miss the view, the ice coffee, my teammates, having shawarma for lunch, breaking and fixing Nagios, joking with my homeboy Lautaro: everyday we said that in our last day we'll steal the coffee machine. (No, I won't steal it. That's a crime)

On the bright side, I got 6 months worth of experience in a very valuable array of tools. I'm grateful to have had a Cloud Operations job as my first working experience in the IT industry. Nagios, Grafana, Kibana, SQL Server, Git, CI/CD pipelines sound great in a CV. Mainly, I appreciate the sandbox environment where I could break and fix stuff (breaking nagios prod and failing deployments made my hands sweaty tho).

## What's next? 🚀

Guess who's back in the job market? Ta-da! Nicolas fucking You.

Luckily, I've been applying to jobs for like 2 weeks. I think I've sent close to 100 applications.

Already got like tens of "We chose a better fitting candidate, thanks tho" but most important: I got a job interview tomorrow and Friday. mmmm bitchesss

That's it for now. Super short read I know, didn't even bother proof reading it. 

Hope you guys are doing better than I. Till the next time ~

![See ya](/post/2023-11-15_seeya.jpg)