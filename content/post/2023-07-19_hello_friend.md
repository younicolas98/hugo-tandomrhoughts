---
title: "Hello, Friend"
date: 2023-07-19T19:39:54-03:00
draft: false
---
![Hello Friend](/post/2023-07-19_Hello_Friend.jpg 'Hello Friend')

Yep, "Hello, Friend". Es una referencia a [_Mr. Robot_](https://open.spotify.com/track/2fFlmlePz9hrVMv4LvdQxN?si=1f053c631c6e44ba). (Tremenda serie, si no la vieron porfis mirenla)


Como verán, empecé un blog. Voy a compartir mis pensamientos en diferentes tópicos: reviews de albums, series o pelis, hablar de mis pasiones, además del ocasional random thought.


Espero que les guste.

_btw, gracias a @arutisutolee por el dibujo_