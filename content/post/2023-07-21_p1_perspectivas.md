---
title: "Yo y mis perspect| |ivas"
subtitle: "Cosas que estuve pensando durante la preparación del retiro"
date: 2023-07-21T00:21:42-03:00
draft: false
tags: ["SD", "Kiosa"]
---
Hoy empieza el retiro espiritual para los chicos de SD. Tengo un par de cosas que pensé durante la prep que les quiero compartir.

# Story time

El otro día tuve una (pequeña) revelación. La oficina del laburo está en el piso 33, lo que significa que tiene una vista increíble. A mi derecha puedo ver Puerto Madero y directamente al frente, el norte de la ciudad. La vista es tan buena que podría pasarme todo el día mirando por la ventana, pero lamentablemente no me pagan por admirar el paisaje de la ciudad. Por el ángulo del sol, generalmente mantenemos las cortinas bajas durante la mayor parte del día.

Ahora pasemos a la revelación. Un día, llegué y no habían bajado las cortinas de la ventana del norte. El cielo estaba 100% despejado, no había una sola nube. Mientras miraba hacia afuera, vi pasar un avión.

Recordemos que estamos en el piso 33, los aviones pasan casi a la altura de los ojos. (En mi casa, en el 4to piso, lo único que veo son autos y gente, así que ver un avión despegar así de cerca fue un treat)

No solo pasó uno, sino que en el lapso de 10 mins vi pasar 3 o 4. Curioso, le digo a mi compañero, "che, ¿que onda que pasan tantos aviones?". Me responde: "yep, allá está el aeroparque". Resulta que durante todo un mes, no tenía idea de que los aviones pasaban cada cinco minutos porque las cortinas siempre estaban bajas.

Ahora, cada vez que llego lo primero que hago es un café (cuz I need that caffeine hit). Lo segundo que hago es (si no molesta a nadie) subir las cortinas de esas dos ventanas y cuando quiero me quedo viendo los aviones despegar y aterrizar.

_End of story_

![View](/post/2023-07-21_view.jpg)
*Vista a mi derecha, no tengo foto de los aviones, sorry*

Bueno, eso fue la revelación: puedo ver aviones (_yayy_). Pero hoy no les vengo a presumir la buena vista que tengo en el trabajo. Quiero hacer énfasis en lo siguiente:

A veces, nos perdemos de cosas (grandes o chiquitas) debido a que no podemos ver más allá de nuestros límites. Sobrepasar o sacar esos límites puede ser tan simple como lo es levantar la cortina. Pero para llegar a esa realización tan simple, muchas veces necesitamos de un input externo. Ese input puede ser una casualidad, como fue el día que encontré las ventanas libres y las cortinas sin bajar.

_Yeah, we get it. ¿Qué tiene que ver esto con el pijong?_

# P1_Perspectivas
![This too shall pass](/post/2023-07-21_this_too_shall_pass.jpeg)
*Lema/logo del pijong*

Esta anécdota la compartí en una de las primeras reuniones de pijong. Quería que los chicos puedan sentir la presencia y el amor de Dios. Y que eso no lo hagamos con una actividad extravagante con tremendo plot-twist. Para apreciar a Dios (si todavía no lo podemos ver) solo necesitamos un cambio de perspectiva.

Esa pequeña palabra "_perspectiva_" terminó siendo el Program 1. La primera dinámica del retiro que setea el ambiente y da pie a todos los otros programas. El propósito es simple: conocernos con actividades rompehielos y darnos cuenta de que el amor de Dios nos rodea en todo momento.

## ¿Cómo lo hicimos?

3 actividades súper simples, cada una con su reflexión.
- Un rolling paper con categorías sobre uno
- Mostrar los vínculos que tengo en mi vida
- Armar un timetable

La más importante es la reflexión del timetable. La actividad en sí es súper simple: escribir y compartir la rutina semanal. Les comparto unos párrafos de la reflexión que le dimos a los chicos:

> En nuestra vida diaria, estamos inmersos en una rutina llena de actividades: estudiar, pasar tiempo con amigos, disfrutar de nuestros hobbies y cumplir con nuestras responsabilidades. A veces, nos dejamos llevar por la inercia y olvidamos que Dios está presente en cada una de estas situaciones.
>
> Al armar el timetable de sus rutinas, los invitamos a reflexionar sobre cómo estamos utilizando nuestro tiempo. [...] Si estamos estudiando, podemos agradecer la oportunidad de aprender y crecer intelectualmente. Si estamos con amigos, podemos agradecer por las relaciones significativas que nos ha regalado. Incluso en los momentos de descanso y ocio, podemos encontrar a Dios en la belleza que nos rodea y en la alegría que sentimos al disfrutar de las cosas simples.
>
> Queremos recordarles que Dios está siempre a nuestro lado, en cada instante de nuestras vidas. Él nos ama incondicionalmente y se preocupa por cada detalle de nuestro ser. Nos acompaña en los momentos de felicidad y también en los momentos difíciles. En las alegrías y en las tristezas, en los éxitos y en los fracasos, Dios está presente, sosteniéndonos y guiándonos en nuestro camino.

De la misma forma que subir las cortinas me permite ver y apreciar el paisaje, ver a Dios requiere a veces subir las cortinas espirituales que no nos permiten sentir su presencia en nuestras vidas diarias.

Creo que nuestro rol como profes es ser capaces de reconocer a los chicos que siguen con su cortina baja y ser el pequeño input que les haga tener su propia realización.

# Conclusión

En fin, espero que les haya gustado mi insight. Los invito a ser ese pequeño input en la vida de las personas que los rodean.


_shoutout to FY por el tí|  |ulo del post_