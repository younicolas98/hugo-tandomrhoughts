---
title: "25 and counting"
subtitle: "Cosas que estuve pensando durante estas semanas"
date: 2023-10-16T15:52:31-03:00
draft: true
tags: ["Random Thoughts"]
---

Buenas, it's been a while ~

Hace un mes aprox que no subo nada, así que este piece va a ser un rejunte de "thoughts" y de paso un pequeño update de mi vida para mis readers ("readers", no son muchos, los puedo contar con mis dedos). No garantizo que el texto esté súper organizado, los voy a escribir en orden cronológico de Octubre -> Septiembre.

Anyways, ahí va. Espero que les guste.

## 25

![Loading](/post/2023-10-16_loading.jpg "Suponiendo que el ser humano llega a los 100")

El otro día cumplí 25. Oficialmente estoy en mis mid-twenties. Cuando tenía veinte, veinticinco me parecía un número muy lejano. 

Me encuentro en el perfecto balance entre tiempo, responsabilidades, conocimiento, salud y money. 
Empezando con este último, I don't have a fuck ton of it (don't need it rn neither), pero tengo los medios para poder hacer las cosas que realmente quiero. Énfasis en hacer y no comprar. No puedo patinarme 1000 dolarucos en una guitarra sin que me duela el bolsillo (aunque me encantaría) ni hablemos de conseguir un auto o una casa. I'm okay tho, el 형님 aka el Honda Accord del 96 se la re banca. Por suerte sí tengo cosas que puedo hacer sin que me duela la billetera: invitar el cafecito en las study seshs, pagar la comida en una ocasión especial, ir a shows de jazz (a veces con artistas internacionales!), planear un buen viaje (este verano no, ya me fui a Portugal). Son cosas que siguen involucrando la $, solo que un much smaller scale.

En cuanto a conocimiento, no sé por qué elegí esta palabra, por ahí "gustos" es una mejor representación de lo que quiero desarrollar. O por ahí es un conocimiento de mí mismo. 

Gustos:

Me gusta mucho el té en hebras y el espresso tonic. I loooove dark chocolate (70% cacao gang rise up). Me gusta mucho cocinar para mi familia. Curiosamente, el acto de comer en sí no me recontra gusta. Creo que es porque no reconozco la diferencia entre algo que "está bueno" y algo "holy fuck, this shit is gooood".

Me gustan los autos viejos; que tengan ángulos rectos, que digan "fuck aerodinamycs"; que sean un poquito incómodos de usar cuz who needs bluetooth audio when you have vi i b e s and a e s t h e t i c s?

[foto de bmw m3]( "Look at this beauty")

Me gusta usar gorras y bucket hats. Desde que tengo el pelo medianamente largo, hay un 85% de probabilidades de que me vean con algo en la cabeza. 

Cómo soy:

-_- o :| Me sigue costando expresar 



### Cumple

### Kiosa

## Jazz

## Luján

## Sarang
