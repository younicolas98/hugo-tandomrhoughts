---
title: "How I write my blog with Git, Hugo and Netlify"
subtitle: "A brief description of how I publish these posts"
date: 2023-07-20T10:39:40-03:00
draft: false
tags: ["Tech", "Blog", "Hugo"]
---

# Intro

I wanted to share with you guys my process of writing and deploying the blog. This isn't a tutorial or a how-to. A post detailing the setup for the blog will come later.


![Screenshot](/post/2023-07-20_workflow_screenshot.png)


# Setup

So, the blog works with [Git](https://git-scm.com/), [Hugo](https://gohugo.io/) and [Netlify](https://www.netlify.com/). 
Git is for version control and working remotely. It allows me to access and edit the blog's code from different devices and locations. Speaking of code, the blog runs on Hugo. It's a static site generator, meaning that I don't need databases or a lot of resources to serve the blog. Netlify automatically builds and deploys changes from my Git repository. 

My current setup works great, plus it's completely free.

# Writing workflow

Before I get to the technical side of the blog, first I need something I want to write about e.g., "how do I write these posts" (_wow, that's so meta!_) or anything related to my passions (more of those post are coming). The topic doesn't need to be mindblowing, I just have to feel the need to share my thoughts on something.

Once I decide on what to write, I open Git Bash, go to the project directory and type:
{{< highlight bash >}}
hugo new post/YYYY-MM-DD_post-title.md
{{< /highlight >}}
I open VSCode to edit the post.md file with the words I like. (I probably should find a markdown editor but vscode works fine for now)
I also run:
{{< highlight bash >}}
hugo serve
{{< /highlight >}}
This fires up a local server where I can look at changes in real time. With this I can make sure everything works and looks fine before commiting.

| ![Hugo Serve](/post/2023-07-20_hugo_serve.png) | ![localhost](/post/2023-07-20_localhost.png) |
|:----------------------------------------------:|:--------------------------------------------:|
|         hugo serve                             |        local server                          |

This whole process of writing and editing takes a whole bunch of time. And the end result is a (at most) 5 min read blog post 🙃. I mean, I've just spent 15 minutes trying to fit two images side by side and the best solution I could find for now is a table (I can't even make one without the borders!!!).

So once the final draft looks good, I push the changes to the repository.
{{< highlight bash >}}
git pull 
git add .
git commit -m "Adding new content"
git status
git push
{{< /highlight >}}

# Netlify auto deploy

Netlify is integrated with the project's Git repo, so it automatically detects whenever I push changes. When changes are detected, Netlify triggers a build process using Hugo. It compiles the site and generates a new version based on the latest changes. After the successful build, Netlify automatically deploys the new version to the live website, making it instantly available to you guys.

# Conclusion

By harnessing the combined power of Git, Hugo, and Netlify, my blog's workflow has become a seamless and enjoyable process. This toolkit allows me to focus on what truly matters – sharing my thoughts, one blog post at a time. I hope these insights inspire you to explore these tools and find your own optimized workflow for blogging.



![gpt](/post/2023-07-20_caracola.png)
_shoutout to la caracóla mágica_
